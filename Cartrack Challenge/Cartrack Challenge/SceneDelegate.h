//
//  SceneDelegate.h
//  Cartrack Challenge
//
//  Created by Ferdinand Lim on 20/7/20.
//  Copyright © 2020 Ferdinand Lim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

