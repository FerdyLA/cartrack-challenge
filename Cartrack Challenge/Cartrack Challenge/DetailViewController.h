//
//  DetailViewController.h
//  Cartrack Challenge
//
//  Created by Ferdinand Lim on 20/7/20.
//  Copyright © 2020 Ferdinand Lim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSDate *detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

