//
//  LoginViewController.h
//  Cartrack Challenge
//
//  Created by Ferdinand Lim on 20/7/20.
//  Copyright © 2020 Ferdinand Lim. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerCountry;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)doneUsername:(id)sender;
- (IBAction)donePassword:(id)sender;
- (IBAction)doLogin:(id)sender;


@end

NS_ASSUME_NONNULL_END
